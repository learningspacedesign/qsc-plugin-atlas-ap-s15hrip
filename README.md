# Atlas IED AP-S15HRIP Power Controller

This plugin uses the Atlas Web Commands and only supports functions detailed in the Atlas IED documentation.  The Web interface of the device is required for some setup parameters.

See [Atlas AP-S15HRIP IP Power Strip Web Commands Documentation](https://www.atlasied.com/f/10467/ATS006167-IP-Power-Strip-Command-Doc.pdf)

## Plugin Information

This version was tested against a AP-S15HRIP Half Rack model running firmware v1.12 and was the basis for inital development.

## Overview

### Control Page
![Control Page](./Help%20File/Atlas%20AP-S15HRIP%20Control%20Page.png)

**Function Description**
- Unit Model (Shows Current Model Info)
  - **Read Only**
- Unit MAC Address (Shows Network Port MAC Address)
  - **Read Only**
- Unit Firmware Version (Shows Current Firmware Version)
  - **Read Only**
- Current Temperature (Shows Current Probe Temperature)
  - **Read Only**
- Current Humidity (Shows Current Probe Humidity Level)
  - **Read Only**
- Input Voltage (Shows Current Input Voltage)
  - **Read Only**
- Current Draw (Shows Current Input Voltage)
  - **Read Only**
- Watts Draw (Shows Current Watts Draw)
  - **Read Only**
- Front Panel Switches (Shows Whether the Front Panel Switches are Enable or Disabled)
  - **Read Only**
- Outlet Name (Show Current Outlet Name)
  - **Read Only**
- Power (Toggles Current Power Status)
  - **Read,Write**
- Cycle (Turns Power Outlet Off and Then Back on In 3 Seconds)
  - **Read,Write**
- State (Show Current Outlet Power State)
  - **Read Only**

### Setup Page
![Setup Page](./Help%20File/Atlas%20AP-S15HRIP%20Setup%20Page.png)

  **Function Description**
- IP Address (Set IP Address Of Atlas AP-S15HRIP Network Port For Plugin To Connect)
  - **Read,Write**
- Connection Status (Show Current Connection Status Of Plugin)
  - **Read Only**