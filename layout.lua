local CurrentPage = PageNames[props["page_index"].Value]
if CurrentPage == "Control" then
  table.insert(graphics,{
    Type = "GroupBox",
    Fill = {200,200,200},
    CornerRadius = 5,
    StrokeWidth = 1,
    Position = {5,5},
    Size = {366,500}
  })
  Logo = "--[[ #encode "logo.svg" ]]"
  table.insert(graphics,{
    Type = "Svg",
    Image = Logo,
    Position = {33,425},
    Size = {300,64},
    ZOrder = 1000
  })
  table.insert(graphics,{
    Type = "Header",
    Text = "Information",
    Fill = {200,200,200},
    Position = {15,15},
    Size = {346,11},
    FontSize = 16,
    Font = "Roboto",
    FontStyle = "Bold",
    FontColor = {27,33,43},
    HTextAlign = "Center",
  })
  table.insert(graphics,{
    Type = "Text",
    Text = "Unit Model:",
    Position = {50,42},
    Size = {120,16},
    FontSize = 12,
    HTextAlign = "Right"
  })
  table.insert(graphics,{
    Type = "Text",
    Text = "Unit Mac Address:",
    Position = {50,62},
    Size = {120,16},
    FontSize = 12,
    HTextAlign = "Right"
  })
  table.insert(graphics,{
    Type = "Text",
    Text = "Unit Firmware Version:",
    Position = {50,82},
    Size = {125,16},
    FontSize = 12,
    HTextAlign = "Right"
  })
  table.insert(graphics,{
    Type = "Text",
    Text = "Current Temperature:",
    Position = {50,112},
    Size = {120,16},
    FontSize = 12,
    HTextAlign = "Right"
  })
  table.insert(graphics,{
    Type = "Text",
    Text = "Current Humidity:",
    Position = {50,132},
    Size = {120,16},
    FontSize = 12,
    HTextAlign = "Right"
  })
  table.insert(graphics,{
    Type = "Text",
    Text = "Input Voltage:",
    Position = {50,162},
    Size = {120,16},
    FontSize = 12,
    HTextAlign = "Right"
  })
  table.insert(graphics,{
    Type = "Text",
    Text = "Current Draw:",
    Position = {50,182},
    Size = {120,16},
    FontSize = 12,
    HTextAlign = "Right"
  })
  table.insert(graphics,{
    Type = "Text",
    Text = "Watts Draw:",
    Position = {50,202},
    Size = {120,16},
    FontSize = 12,
    HTextAlign = "Right"
  })
  table.insert(graphics,{
    Type = "Text",
    Text = "Front Panel Switches:",
    Position = {50,222},
    Size = {120,16},
    FontSize = 12,
    HTextAlign = "Right"
  })
  table.insert(graphics,{
    Type = "Label",
    Text = "Version "..PluginInfo.Version,
    HTextAlign = "Right",
    FontSize = 9,
    Position = {300,485},
    Size = {66,16},
  })
  layout["UnitModel"] = {
    PrettyName = "Unit Information~Unit Model",
    Style = "Indicator",
    Position = {175,42},
    Size = {180,16},
    Color = {255,255,255},
    FontSize = 14,
    HTextAlign = "Center",
    IsReadOnly = true
  }
  layout["UnitMacAddress"] = {
    PrettyName = "Unit Information~Unit Mac Address",
    Style = "Text",
    Position = {175,62},
    Size = {180,16},
    Color = {255,255,255},
    FontSize = 14,
    HTextAlign = "Center",
    IsReadOnly = true
  }
  layout["UnitFirmware"] = {
    PrettyName = "Unit Information~Unit Firmware",
    Style = "Text",
    Position = {175,82},
    Size = {180,16},
    Color = {255,255,255},
    FontSize = 14,
    HTextAlign = "Center",
    IsReadOnly = true
  }
  layout["Temp_Sensor"] = {
    PrettyName = "Unit Stats~Current Temp",
    Style = "Text",
    Position = {175,112},
    Size = {180,16},
    Color = {255,255,255},
    FontSize = 14,
    HTextAlign = "Center",
    IsReadOnly = true
  }
  layout["Humidity_Sensor"] = {
    PrettyName = "Unit Stats~Current Humidity",
    Style = "Text",
    Position = {175,132},
    Size = {180,16},
    Color = {255,255,255},
    FontSize = 14,
    HTextAlign = "Center",
    IsReadOnly = true
  }
  layout["AC_Mains_Input_Volts"] = {
    PrettyName = "Unit Stats~Input Voltage",
    Style = "Text",
    Position = {175,162},
    Size = {180,16},
    Color = {255,255,255},
    FontSize = 14,
    HTextAlign = "Center",
    IsReadOnly = true
  }
  layout["AC_Mains_Current_Draw"] = {
    PrettyName = "Unit Stats~Current Draw",
    Style = "Text",
    Position = {175,182},
    Size = {180,16},
    Color = {255,255,255},
    FontSize = 14,
    HTextAlign = "Center",
    IsReadOnly = true
  }
  layout["AC_Mains_Watts_Draw"] = {
    PrettyName = "Unit Stats~Watts Draw",
    Style = "Text",
    Position = {175,202},
    Size = {180,16},
    Color = {255,255,255},
    FontSize = 14,
    HTextAlign = "Center",
    IsReadOnly = true
  }
  layout["FrontPanelSwitches"] = {
    PrettyName = "Unit Stats~Front Panel Switches Enable/Disable",
    Style = "Text",
    Position = {175,222},
    Size = {180,16},
    Color = {255,255,255},
    FontSize = 14,
    HTextAlign = "Center",
    IsReadOnly = true
  }

  --Group Boxes for Outlet Controls
  table.insert(graphics,{
    Type = "Header",
    Text = "Outlets",
    Fill = {200,200,200},
    Position = {15,262},
    Size = {346,11},
    FontSize = 16,
    Font = "Roboto",
    FontStyle = "Bold",
    FontColor = {27,33,43},
    HTextAlign = "Center",
  })
  table.insert(graphics,{
    Type = "Label",
    Text = "Outlet",
    Position = {10,284},
    Size = {42,15},
    FontSize = 12,
    Font = "Roboto",
    FontColor = {51,51,51},
    HTextAlign = "Center",
  })
  table.insert(graphics,{
    Type = "Label",
    Text = "Name",
    Position = {130,284},
    Size = {42,15},
    FontSize = 12,
    Font = "Roboto",
    FontColor = {51,51,51},
    HTextAlign = "Center",
  })
  table.insert(graphics,{
    Type = "Label",
    Text = "Power",
    Position = {236,284},
    Size = {42,15},
    FontSize = 12,
    Font = "Roboto",
    FontColor = {51,51,51},
    HTextAlign = "Center",
  })
  table.insert(graphics,{
    Type = "Label",
    Text = "Cycle",
    Position = {281,284},
    Size = {42,15},
    FontSize = 12,
    Font = "Roboto",
    FontColor = {51,51,51},
    HTextAlign = "Center",
  })
  table.insert(graphics,{
    Type = "Label",
    Text = "State",
    Position = {325,284},
    Size = {42,15},
    FontSize = 12,
    Font = "Roboto",
    FontColor = {51,51,51},
    HTextAlign = "Center",
  })
  --Outlet Controls
  for i=1,5 do
    table.insert(graphics,{
      Type = "Text",
      Text = string.format("%i",i),
      Position = {21,i*22+282},
      Size = {16,16},
      FontSize = 12,
      HTextAlign = "Right"
    })
    layout["Outlet_Name "..i] = {
      PrettyName = string.format("Outlets~Outlet %i~Name", i),
      Style = "Text",
      TextBoxStyle = "Normal",
      Position = {80,i*22+282},
      Size = {150,16},
      Color = {255,255,255},
      FontSize = 14,
      HTextAlign = "Center",
      IsReadOnly = true
    }
    layout["Power "..i] = {
      PrettyName = string.format("Outlets~Outlet %i~Power", i),
      Style = "Button",
      ButtonStyle = "Trigger",
      ButtonVisualStyle = "Flat",
      CornerRadius = 2,
      Margin = 1,
      Position = {240,i*22+282},
      Size = {36,16},
      Color = {124,124,124},
    }
    layout["Power_Cycle "..i] = {
      PrettyName = string.format("Outlets~Outlet %i~Power Cycle", i),
      Style = "Button",
      ButtonStyle = "Trigger",
      ButtonVisualStyle = "Flat",
      CornerRadius = 2,
      Margin = 1,
      Position = {285,i*22+282},
      Size = {36,16},
      Color = {124,124,124},
    }
    layout["Outlet_Status "..i] = {
      PrettyName = string.format("Outlets~Outlet %i~Status", i),
      Style = "Led",
      Position = {340,i*22+282},
      Size = {16,16},
      Color = {255,0,0},
      UnlinkOffColor = false,
      Margin = 3,
      IsReadOnly = true,
    }
  end
elseif CurrentPage == "Setup" then
  table.insert(graphics,{
    Type = "GroupBox",
    Fill = {200,200,200},
    CornerRadius = 5,
    StrokeWidth = 1,
    Position = {5,5},
    Size = {366,250}
  })
  table.insert(graphics,{
    Type = "Header",
    Text = "Setup",
    Fill = {200,200,200},
    Position = {15,15},
    Size = {346,11},
    FontSize = 16,
    Font = "Roboto",
    FontStyle = "Bold",
    FontColor = {27,33,43},
    HTextAlign = "Center",
  })
  table.insert(graphics,{
    Type = "Text",
    Text = "IP Address:",
    Position = {10,42},
    Size = {120,16},
    FontSize = 12,
    HTextAlign = "Right"
  })
  table.insert(graphics,{
    Type = "Text",
    Text = "Connection Status:",
    Position = {10,62},
    Size = {120,16},
    FontSize = 12,
    HTextAlign = "Right"
  })
  Logo = "--[[ #encode "logo.svg" ]]"
  table.insert(graphics,{
    Type = "Svg",
    Position = {33,180},
    Size = {300,64},
    Image = Logo,
    ZOrder = 1000
  })
  table.insert(graphics,{
    Type = "Label",
    Text = "Version "..PluginInfo.Version,
    HTextAlign = "Right",
    FontSize = 9,
    Position = {300,235},
    Size = {66,16},
  })
  layout["IPAddress"] = {
    PrettyName = "Connection Info~Unit IP Address",
    Style = "Text",
    Position = {135,42},
    Size = {150,16},
    Color = {255,255,255},
    FontSize = 12,
    HTextAlign = "Center",
    IsReadOnly = false
  }
  layout["Status"] = {
    PrettyName = "Connection Info~Connection Status",
    Style = "Indicator",
    Position = {135,62},
    Size = {150,100},
    Color = {255,255,255},
    FontSize = 16,
    HTextAlign = "Center",
    IsReadOnly = true
  }
end
