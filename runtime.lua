require("LuaXML")

--Debug Function
---------------------------------------------------------
function DebugFormat(string) -- Format strings containing non-printable characters so we can see what they are
  local visual = ""
  for i=1,#string do
    local byte = string:sub(i,i)
    if string.byte(byte) >= 32 and string.byte(byte) <= 126 then
      visual = visual..byte
    else
      visual = visual..string.format("[%02xh]",string.byte(byte))
    end
  end
  return visual
end
DebugTx = false
DebugRx = false
DebugFunction = false
DebugPrint = Properties["Debug Print"].Value

-- A function to determine common print statement scenarios for troubleshooting
function SetupDebugPrint()
  if DebugPrint=="Tx/Rx" then
    DebugTx,DebugRx=true,true
  elseif DebugPrint=="Tx" then
    DebugTx=true
  elseif DebugPrint=="Rx" then
    DebugRx=true
  elseif DebugPrint=="Function Calls" then
    DebugFunction=true
  elseif DebugPrint=="All" then
    DebugTx,DebugRx,DebugFunction=true,true,true
  end
end

--TCP Socket Event Handlers
-----------------------------------------------------------
function StatusChange(lsErrCode, lsErr)
if(lsErrCode == 200)then
  if(DebugTx)then print("Web Request Success")end
  Controls["Status"].Value = 0
  Controls["Status"].String = ''
elseif(lsErrCode == 0)then
  if(DebugTx)then print("%s", Err)end
  Controls["Status"].Value = 2
  Controls["Status"].String = lsErr
end
end

---Controlling Outlets
---------------------------------------------------------
gsOutletOn = "1"
gsOutletOff = "0"

function OutletControlResponse(tbl, code, d, e)
  if(DebugRx)then print(string.format("Outlet Control Response Code: %i\t\tErrors: %s\rData: %s",code, e or "None", d))end
end

function OutletControl(lsOutletNum,lsOnOff)
  local BaseURL = string.format("http://%s/?sw%s=%s", gsIPAddress, tostring(lsOutletNum), lsOnOff)
  HttpClient.Upload {
    Url = BaseURL.."/post",
    Method = "POST",
    Data = "",
    Headers = {
      ["Content-Type"] = "text/html",
    },
    EventHandler = OutletControlResponse -- The function to call upon response
  }
end

for x, Outlets in pairs (Controls["Power"]) do
  Outlets.EventHandler = function(lbCtrl)
    if(Controls["Outlet_Status"][x].Boolean == true)then --if outlet on turn off
      OutletControl((x + 3), gsOutletOff)
      Timer.CallAfter(GetDeviceStatus, 1)
    else                                                 --if outlet off turn on
      OutletControl((x + 3), gsOutletOn)
      Timer.CallAfter(GetDeviceStatus, 1)
    end
  end
end

autoTurnOutletOn = Timer.New()
gsCurrentOutletPressed = 0

autoTurnOutletOn.EventHandler = function()
  if(DebugFunction)then print("Auto Turn On Fired")end
  OutletControl(gsCurrentOutletPressed, gsOutletOn)
  autoTurnOutletOn:Stop()
end

for x, Outlets in pairs (Controls["Power_Cycle"]) do
  Outlets.EventHandler = function(lbCtrl)
      OutletControl((x + 3), gsOutletOff)
      gsCurrentOutletPressed = (x + 3)
      Timer.CallAfter(GetDeviceStatus, 1)
      autoTurnOutletOn:Start(3)
      Timer.CallAfter(GetDeviceStatus, 4)
  end
end

--Parsing Data
--------------------------------------------------------
function removeXMLHeaders(lsData)
  local lsStart = string.find(lsData, ">")
  local lsEnd = string.find(lsData, "</")
  local lsFilteredString = string.sub(lsData, (lsStart + 1), (lsEnd - 1))

    if(DebugFunction)then print(lsFilteredString)end

  return(lsFilteredString)
end

function ResponseParser(lsResponse)

  if(lsResponse ~= nil)then
    responsetable = xml.eval(lsResponse)      --Take Raw data and convert it from XML to Lua Table
    
    if(xml.find(responsetable,"FW") ~= nil)then --Make Sure There is data in Table
      Controls["UnitFirmware"].String = removeXMLHeaders(tostring(xml.find(responsetable,"FW")))    --Format String and send it to Controls
    end
    
    if(xml.find(responsetable,"MAC") ~= nil)then --Make Sure There is data in Table    
      Controls["UnitMacAddress"].String = removeXMLHeaders(tostring(xml.find(responsetable,"MAC")))
    end
    if(xml.find(responsetable,"type") ~= nil)then --Make Sure There is data in Table
      Controls["UnitModel"].String = removeXMLHeaders(tostring(xml.find(responsetable,"type")))
    end
    if(xml.find(responsetable,"temp") ~= nil)then --Make Sure There is data in Table
      Controls["Temp_Sensor"].String = removeXMLHeaders(tostring(xml.find(responsetable,"temp")))
    end
    if(xml.find(responsetable,"humi") ~= nil)then --Make Sure There is data in Table    
    Controls["Humidity_Sensor"].String = removeXMLHeaders(tostring(xml.find(responsetable,"humi")))
    end
    if(xml.find(responsetable,"volt") ~= nil)then --Make Sure There is data in Table    
    Controls["AC_Mains_Input_Volts"].String = removeXMLHeaders(tostring(xml.find(responsetable,"volt")))
    end
    if(xml.find(responsetable,"curr") ~= nil)then --Make Sure There is data in Table    
    Controls["AC_Mains_Current_Draw"].String = removeXMLHeaders(tostring(xml.find(responsetable,"curr")))
    end
    if(xml.find(responsetable,"watt") ~= nil)then --Make Sure There is data in Table    
    Controls["AC_Mains_Watts_Draw"].String = removeXMLHeaders(tostring(xml.find(responsetable,"watt")))
    end
    if(xml.find(responsetable,"PANEL") ~= nil)then --Make Sure There is data in Table    
      if(removeXMLHeaders(tostring(xml.find(responsetable,"PANEL"))) == "0")then
        Controls["FrontPanelSwitches"].String = "Disabled"
      elseif(removeXMLHeaders(tostring(xml.find(responsetable,"PANEL"))) == "1")then
        Controls["FrontPanelSwitches"].String = "Enabled"
      end
    end
    
    for x, lbOutlets in pairs (Controls["Outlet_Name"]) do
      if(xml.find(responsetable, string.format("sw%sO", (x + 3))) ~= nil)then --Make Sure There is data in Table 
        Controls["Outlet_Name"][x].String = removeXMLHeaders(tostring(xml.find(responsetable,string.format("sw%sO", (x + 3)))))
      end
      
      if(xml.find(responsetable, string.format("sw%s", (x + 3))) ~= nil)then --Make Sure There is data in Table  
        Controls["Outlet_Status"][x].String = removeXMLHeaders(tostring(xml.find(responsetable,string.format("sw%s", (x + 3)))))
      end    
    end
  end
end
---Get Device Settings
---------------------------------------------------------
function DeviceSettingResponse(tbl, code, data, err, headers)
  if(DebugRx)then print(string.format( "HTTP response from '%s': Return Code=%i; Error=%s", tbl.Url, code, err or "None" ) )end
  StatusChange(code, err)
  
  if(DebugRx)then print("Headers:")end
  for hName,Val in pairs(headers) do
    if(DebugRx)then print(string.format( "\t%s = %s", hName, Val ) )end
  end
  ResponseParser(data)
  if(DebugRx)then print( "\rHTML Data: "..data )end
end

function GetDeviceSetting()
  local SettingsBaseURL = string.format("http://%s/?Settings", gsIPAddress)
  HttpClient.Download { Url = SettingsBaseURL, Headers = { ["Content-Type"] = "ext/xml" } , Timeout = 5, EventHandler = DeviceSettingResponse }
end

---Get Device Status
---------------------------------------------------------
function DeviceStatusResponse(tbl, code, data, err, headers)
  if(DebugRx)then print(string.format( "HTTP response from '%s': Return Code=%i; Error=%s", tbl.Url, code, err or "None" ) )end
  StatusChange(code, err)
  if(DebugRx)then print("Headers:")end
  for hName,Val in pairs(headers) do
    if(DebugRx)then print(string.format( "\t%s = %s", hName, Val ) )end
  end
  ResponseParser(data)
  if(DebugRx)then print( "\rHTML Data: "..data )end
end

function GetDeviceStatus()
  local StatusBaseURL = string.format("http://%s/?Status", gsIPAddress)
  HttpClient.Download { Url = StatusBaseURL, Headers = { ["Content-Type"] = "ext/xml" } , Timeout = 5, EventHandler = DeviceStatusResponse }
end


--Timer
---------------------------------------------------------
QueryTimer = Timer.New()

QueryTimer.EventHandler = function()
  if(DebugTx)then print("Query Sent")end
  GetDeviceSetting()
  Timer.CallAfter(GetDeviceStatus, 2)
end 

--IP Address Saving
------------------------------------------------------------
gsIPAddress = Controls["IPAddress"].String

if(gsIPAddress ~= "")then
  GetDeviceSetting()
  Timer.CallAfter(GetDeviceStatus, 2)
  QueryTimer:Start(15)
else
  QueryTimer:Stop()
  Controls["Status"].Value = 2
  Controls["Status"].String = "No Valid IP Entered"
end


Controls["IPAddress"].EventHandler = function()
    gsIPAddress = Controls["IPAddress"].String
    
    if(gsIPAddress ~= "")then
      GetDeviceSetting()
      Timer.CallAfter(GetDeviceStatus, 2)
      QueryTimer:Start(15)
    else
      QueryTimer:Stop()
      Controls["Status"].Value = 2
      Controls["Status"].String = "No Valid IP Entered"
    end
end

--Initialization
SetupDebugPrint()