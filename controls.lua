table.insert(ctrls, {
  Name = "UnitModel",
  ControlType = "Text",
  Count = 1,
  UserPin = true,
  PinStyle = "Output",
})
table.insert(ctrls, {
  Name = "UnitMacAddress",
  ControlType = "Text",
  Count = 1,
  UserPin = true,
  PinStyle = "Output",
})
table.insert(ctrls, {
  Name = "UnitFirmware",
  ControlType = "Text",
  Count = 1,
  UserPin = true,
  PinStyle = "Output",
})
table.insert(ctrls, {
  Name = "Temp_Sensor",
  ControlType = "Text",
  Count = 1,
  UserPin = true,
  PinStyle = "Output",
})
table.insert(ctrls, {
  Name = "Humidity_Sensor",
  ControlType = "Text",
  Count = 1,
  UserPin = true,
  PinStyle = "Output",
})
table.insert(ctrls, {
  Name = "AC_Mains_Input_Volts",
  ControlType = "Text",
  Count = 1,
  UserPin = true,
  PinStyle = "Output",
})
table.insert(ctrls, {
  Name = "AC_Mains_Current_Draw",
  ControlType = "Text",
  Count = 1,
  UserPin = true,
  PinStyle = "Output",
})
table.insert(ctrls, {
  Name = "AC_Mains_Watts_Draw",
  ControlType = "Text",
  Count = 1,
  UserPin = true,
  PinStyle = "Output",
})
table.insert(ctrls, {
  Name = "FrontPanelSwitches",
  ControlType = "Text",
  Count = 1,
  UserPin = true,
  PinStyle = "Output",
})
table.insert(ctrls, {
  Name = "Outlet_Name",
  ControlType = "Text",
  Count = 5,
  UserPin = true,
  PinStyle = "Output",
})
table.insert(ctrls, {
  Name = "Power",
  ControlType = "Button",
  ButtonType = "Trigger",
  Count = 5,
  UserPin = true,
  PinStyle = "Both",
})
table.insert(ctrls, {
  Name = "Power_Cycle",
  ControlType = "Button",
  ButtonType = "Trigger",
  Count = 5,
  UserPin = true,
  PinStyle = "Both",
})
table.insert(ctrls, {
  Name = "Outlet_Status",
  ControlType = "Indicator",
  IndicatorType = "Led",
  Count = 5,
  UserPin = true,
  PinStyle = "Output",
})
table.insert(ctrls, {
  Name = "IPAddress",
  ControlType = "Text",
  Count = 1,
  UserPin = true,
  PinStyle = "Both",
})
table.insert(ctrls, {
  Name = "Status",
  ControlType = "Indicator",
  IndicatorType = "Status",
  Count = 1,
  UserPin = true,
  PinStyle = "Output",
})